VCFLib VCF Filter App
=====================

Version: 1.0.1-01

This GeneFlow2 app wraps the VCFLib VCF Filter function.

Inputs
------

1. input: Input VCF File

Parameters
----------

1. output: Output VCF File
